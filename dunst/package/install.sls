# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as dunst with context %}

dunst-package-install-pkg-installed:
  pkg.installed:
    - name: {{ dunst.pkg.name }}
