# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as dunst with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('dunst-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_dunst', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

dunst-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

dunst-config-file-dunst-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/dunst
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - dunst-config-file-user-{{ name }}-present

dunst-config-file-dunst-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/dunst/dunstrc
    - source: {{ files_switch([
                  name ~ '-dunstrc.tmpl',
                  'dunstrc.tmpl'],
                lookup='dunst-config-file-dunst-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - dunst-config-file-dunst-dir-{{ name }}-managed

{% endif %}
{% endfor %}
{% endif %}
