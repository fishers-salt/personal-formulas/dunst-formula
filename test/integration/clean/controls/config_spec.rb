# frozen_string_literal: true

control 'dunst-config-clean-dunst-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/dunst') do
    it { should_not exist }
  end
end

control 'dunst-config-clean-dunst-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/dunst/dunstrc') do
    it { should_not exist }
  end
end
