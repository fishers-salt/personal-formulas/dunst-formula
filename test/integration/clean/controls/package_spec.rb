# frozen_string_literal: true

control 'dunst-package-clean-pkg-absent' do
  title 'should not be installed'

  describe package('dunst') do
    it { should_not be_installed }
  end
end
