# frozen_string_literal: true

control 'dunst-package-install-pkg-installed' do
  title 'should be installed'

  describe package('dunst') do
    it { should be_installed }
  end
end
