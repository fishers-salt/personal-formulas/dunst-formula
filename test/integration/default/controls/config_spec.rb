# frozen_string_literal: true

control 'dunst-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'dunst-config-file-dunst-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/dunst') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'dunst-config-file-dunst-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/dunst/dunstrc') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') do
      should include('# Which monitor should the notifications be displayed on.')
    end
  end
end
